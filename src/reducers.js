import { combineReducers } from 'redux-immutable';

import files from 'src/modules/files/reducers'

const root = (reducers) => {
    return combineReducers({
        files
    })
}

export default root;
