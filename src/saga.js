import { all } from 'redux-saga/effects';
import services from 'src/services';
import config from 'src/config';

import {FilesRepository} from "./repositories/files.repository";

export const getRootSaga = (store) => {
    return function* rootSaga() {

        const files = new FilesRepository(services, config, store)

        yield all([
            ...files.init()
        ])
    }
}
