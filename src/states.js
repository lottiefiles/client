import { fromJS } from 'immutable';

export default fromJS({
    files: {
        list: {
            loading: false,
            data: []
        },
        details: {
            loading: false,
            data: {}
        }
    }
})
