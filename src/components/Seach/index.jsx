import React, { useState } from "react";

export const Search = ({ onSearch }) => {

    const [value, setValue] = useState('')

    const onChange = (event) => {
        setValue(event.target.value || '')
    }

    const onSubmit = (event) => {
        event.preventDefault()
        if (typeof onSearch === 'function') {
            onSearch(value)
        }
    }

    return (
            <form className="container flex justify-end" onSubmit={onSubmit}>
                <div className="relative">
                    <div className="absolute top-5 left-3">
                        <i className="fa fa-search text-gray-400 z-20 hover:text-gray-500" />
                    </div>
                    <input
                        type="text"
                        className="h-10 w-96 pl-5 pr-20 rounded-sm z-0 focus:shadow focus:outline-none"
                        placeholder="Search anything..."
                        value={value}
                        onChange={onChange}
                    />
                    <div className="absolute top-1 right-1">
                        <button
                            type="submit"
                            className="h-8 w-20 text-white rounded-sm bg-blue-400 hover:bg-red-600">Search</button>
                    </div>
                </div>
            </form>
        // <div className="search-input w-full mr-0 px-4 md:px-0">
        //     <div className="flex-1 ais-InstantSearch">
        //         <div className="ais-Autocomplete">
        //             <div>
        //                 <div className="text-sm flex-1">
        //                     <div className="outline-none bg-purple-white rounded-full w-full border-0 md:p-3 pb-2 relative">
        //                         <input
        //                             id="searchinput"
        //                             type="search"
        //                             name="q"
        //                             autoComplete="off"
        //                             placeholder="Search Lottie Animations"
        //                             className="search_field trans trans-slow outline-none text-sm transition hover:bg-grey-light focus:bg-lf-teal focus:font-bold bg-purple-white rounded-lg shadow-sm w-full border-0 p-3 pl-10"
        //                             value={value}
        //                             onChange={onChange}
        //                         />
        //                         <svg
        //                             version="1.1"
        //                             xmlns="http://www.w3.org/2000/svg"
        //                             x="0px"
        //                             y="0px"
        //                             viewBox="0 0 52.966 52.966"
        //                             className="h-4 text-dark absolute text-grey-darker pin-l pin-t opacity-50 mx-4 my-3 md:mx-6 md:my-6"
        //                         >
        //                             <path d="M51.704,51.273L36.845,35.82c3.79-3.801,6.138-9.041,6.138-14.82c0-11.58-9.42-21-21-21s-21,9.42-21,21s9.42,21,21,21
        //                 c5.083,0,9.748-1.817,13.384-4.832l14.895,15.491c0.196,0.205,0.458,0.307,0.721,0.307c0.25,0,0.499-0.093,0.693-0.279
        //                 C52.074,52.304,52.086,51.671,51.704,51.273z M21.983,40c-10.477,0-19-8.523-19-19s8.523-19,19-19s19,8.523,19,19
        //                 S32.459,40,21.983,40z" />
        //                         </svg>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //         <form id="searchform" method="get" action="/search">
        //             <input id="hiddensearchinput" name="q" hidden="hidden" />
        //             <input id="searchcategory" name="category" value="animations" hidden="hidden" />
        //         </form>
        //     </div>
        // </div>
    )
}
