import React from 'react';
import Image from "next/image";

export const TopBar = () => {
    return (
        <div className="grid grid-cols-1 p-10">
            <nav className="container flex overflow-y-hidden scrollbar-hidden p-15">
                <Image className="items-center" src="/assets/images/logo.png" width={216} height={44}/>
            </nav>
        </div>
    )
}
