import {createAction} from "@reduxjs/toolkit";
import * as constants from './constants'

export const list = {
    fetch: createAction(constants.FETCH_ALL_FILES),
    fetching: createAction(constants.FETCHING_ALL_FILES),
    update: createAction(constants.UPDATE_ALL_FILES)
}

export const details = {
    fetch: createAction(constants.FETCH_FILE),
    fetching: createAction(constants.FETCHING_FILE),
    update: createAction(constants.UPDATE_FILE)
}
