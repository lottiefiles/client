import getAllFiles from './getAllFiles.graphql'
import getFileDetails from './getFileDetails.graphql'

export default {
    getAllFiles,
    getFileDetails
}

