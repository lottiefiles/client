const ENTITY = 'FILES';

export const FETCH_ALL_FILES = `${ENTITY}/FETCH_ALL`;
export const FETCHING_ALL_FILES = `${ENTITY}/FETCHING_ALL`;
export const UPDATE_ALL_FILES = `${ENTITY}/UPDATE_ALL`;

export const FETCH_FILE = `${ENTITY}/FETCH`;
export const FETCHING_FILE = `${ENTITY}/FETCHING`;
export const UPDATE_FILE = `${ENTITY}/UPDATE`
