import {createSelector} from "reselect";

const FilesSelector = () => state => state.get('files')

const FilesListSelector = () =>
    createSelector(
        FilesSelector(),
        state => state.get('list')
    )

const FilesDetailsSelector = () =>
    createSelector(
        FilesSelector(),
        state => state.get('details')
    )


export const getFiles = () =>
    createSelector(
        FilesListSelector(),
        state => [
            Boolean(state.get('loading')),
            state.get('data').toJS()
        ]
    )

export const getDetails = () =>
    createSelector(
        FilesDetailsSelector(),
        state => [
            Boolean(state.get('loading')),
            state.get('data').toJS()
        ]
    )
