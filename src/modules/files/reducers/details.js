import { createReducer } from '@reduxjs/toolkit';
import { fromJS } from 'immutable';
import states from 'src/states'
import * as constants from '../constants';

export default createReducer(states.get('files').get('details').toJS(), {
    [constants.FETCHING_FILE]: (state, {payload}) => state.set('loading', payload),
    [constants.UPDATE_FILE]: (state, {payload}) => state.set('data', fromJS(payload))
})
