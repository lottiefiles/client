import { createReducer } from '@reduxjs/toolkit';
import { fromJS } from 'immutable';
import states from 'src/states'
import * as constants from '../constants';

export default createReducer(states.get('files').get('list').toJS(), {
    [constants.FETCHING_ALL_FILES]: (state, {payload}) => state.set('loading', payload),
    [constants.UPDATE_ALL_FILES]: (state, {payload}) => state.set('data', fromJS(payload))
})
