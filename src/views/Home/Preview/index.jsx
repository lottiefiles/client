import React, { useEffect, useRef, useState } from "react";
import {useDispatch, useSelector} from "react-redux";
import files from 'src/modules/files'
import config from "src/config";

export const Preview = () => {
    const ref = useRef(null)
    const [loading, data] = useSelector(files.selectors.getDetails())
    const [speed, setSpeed] = useState(1)
    const [background, setBackground] = useState('#000')
    const dispatch = useDispatch()

    const changeSpeed = (value) => () => {
        setSpeed(value)
    }

    const changeBackground = (value) => () => {
        setBackground(value)
    }

    const close = () => {
        dispatch(files.actions.details.update({}))
    }

    if (Object.keys(data).length === 0) {
        return null
    }

    if (loading) {
        return 'loading ..'
    }



    return (
        <div className="modal fixed w-full h-full top-0 left-0 flex items-center justify-center modal-active">
            <div className="modal-overlay absolute w-full h-full bg-white opacity-95" />

            <div className="modal-container fixed w-full h-full z-50 overflow-y-auto ">

                <div
                    onClick={close}
                    className="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-black text-sm z-50">
                    <svg className="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                        viewBox="0 0 18 18">
                        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z" />
                    </svg>
                </div>

                <div className="modal-content container mx-auto h-auto text-left p-10">

                    <div className="grid grid-cols-2">
                        <div>
                            <lottie-player
                                id="firstLottie"
                                ref={ref}
                                controls
                                autoplay
                                loop
                                mode="normal"
                                background={background}
                                speed={speed}
                                src={`${config.common.files.url}${data.filePath}`}
                                style={{ width: "600px", height: "600px" }}
                            />
                        </div>
                        <div>
                            <h1 className="text-4xl mb-10">
                                {data.title}
                                {data.featured &&
                                 <span className="inline-block ml-5 bg-green-200 rounded-full px-3 py-1 text-sm font-semibold text-white mr-2 mb-2">Featured</span>
                                }
                            </h1>
                            <nv className="mb-10 mb-5 block">
                                <h3 className="text-xl mb-2">Speed :</h3>
                                <button
                                    className="mr-2 inline-block border border-black text-grey-dark rounded p-3"
                                    onClick={changeSpeed(1)}
                                >1x</button>
                                <button
                                    className="mr-2 inline-block border border-black text-grey-dark rounded p-3"
                                    onClick={changeSpeed(2)}
                                >2x</button>
                                <button
                                    className="mr-2 inline-block border border-black text-grey-dark rounded p-3"
                                    onClick={changeSpeed(3)}
                                >3x</button>
                                <button
                                    className="mr-2 inline-block border border-black text-grey-dark rounded p-3"
                                    onClick={changeSpeed(4)}
                                >4x</button>
                                <button
                                    className="mr-2 inline-block border border-black text-grey-dark rounded p-3"
                                    onClick={changeSpeed(5)}
                                >5x</button>
                            </nv>
                            <nav>
                                <h3 className="text-xl mb-2">Background Color :</h3>
                                <button
                                    className="bg-black mr-2 inline-block border border-black text-grey-dark rounds-50p p-3"
                                    onClick={changeBackground('black')}
                                />
                                <button
                                    className="bg-white mr-2 inline-block border border-black text-grey-dark rounds-50p p-3"
                                    onClick={changeBackground('white')}
                                />
                                <button
                                    className="bg-gray-500 mr-2 inline-block border border-black text-grey-dark rounds-50p p-3"
                                    onClick={changeBackground('grey')}
                                />
                                <button
                                    className="bg-red-500 mr-2 inline-block border border-black text-grey-dark rounds-50p p-3"
                                    onClick={changeBackground('red')}
                                />
                                <button
                                    className="bg-green-500 mr-2 inline-block border border-black text-grey-dark rounds-50p p-3"
                                    onClick={changeBackground('green')}
                                />
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}
