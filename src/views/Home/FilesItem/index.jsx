import React, {useEffect, useRef} from "react";
import config from 'src/config'
import files from 'src/modules/files'
import Link from 'next/link'
import {useDispatch} from "react-redux";


export const FileItem = ({item}) => {
    const ref = useRef(null);
    const dispatch = useDispatch()

    const onClick = (id) => () => {
        dispatch(files.actions.details.fetch(id))
    }

    return (
        <div className="rounded overflow-hidden shadow-lg bg-white">

            <lottie-player
                id="firstLottie"
                ref={ref}
                autoplay
                loop
                mode="normal"
                src={`${config.common.files.url}${item.filePath}`}
                style={{width: "465px", height: "368px"}}
            />
            <div className="px-6 py-4">
                <div className="font-bold text-xl mb-2">
                    {item.title}
                    <button className="inline-block float-right bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2" onClick={onClick(item.fId)}>Preview</button>
                </div>
            </div>
        </div>
    )
}
