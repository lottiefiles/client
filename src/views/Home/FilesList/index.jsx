import React from "react";
import {useSelector} from "react-redux";
import files from 'src/modules/files'
import {FileItem} from "../FilesItem";

export const FilesList = () => {
    const [loading, data]= useSelector(files.selectors.getFiles())

    return (
        <div className="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
        {loading && 'loading...'}
            {data.map(item => (
                <FileItem key={item.fid} item={item} />
            ))}
        </div>
    )
}
