import React, {useEffect, useState} from 'react';
import {func} from 'prop-types';
import {Search} from "src/components/Seach";
import cx from 'classnames';

export const FilterBar = ({ onChange }) => {

    const [filter, setFilter] = useState('')
    const [search, setSearch] = useState('')

    const onChangeFilter = (value) => () => {
        setFilter(value)
    }

    const onChangeSearch = (value) => {
        setSearch(value)
    }

    useEffect(() => {
        onChange({
            search,
            filter
        })
    }, [search, filter])

    return (
        <div className="grid grid-cols-2 px-10">
            <div className="text-left items-center flex">
            <div>
                <button
                    onClick={onChangeFilter('')}
                    className={cx(
                        'mr-2',
                        'inline-block',
                        'border',
                        'border-blue',
                        'text-grey-dark',
                        'rounded',
                        'bg-white',
                        'px-5',
                        'py-2',                      {
                        'bg-green-500': filter === ''
                        }
                    )}
                >
                    <span>All</span>
                </button>
                <button
                    onClick={onChangeFilter('featured')}
                    className={cx(
                        'mr-2',
                            'inline-block',
                            'border',
                            'border-blue',
                            'text-grey-dark',
                            'rounded',
                            'bg-white',
                            'px-5',
                            'py-2',
                            {
                                'bg-green-500': filter === 'featured'
                            }
                        )}
                >
                    <span>Featured</span>
                </button>
                <button
                    onClick={onChangeFilter('recent')}
                    className={cx(
                    'mr-2',
                    'inline-block',
                    'border',
                    'border-blue',
                    'text-grey-dark',
                    'rounded',
                    'bg-white',
                    'px-5',
                    'py-2',
                    {
                        'bg-green-500': filter === 'recent'
                    }
                )}>
                    <span>Recent</span>
                </button>
            </div>
            </div>
            <div className="text-left content-center">
                <Search onSearch={onChangeSearch} />
            </div>
        </div>
    )
}
