export class BaseRepository {
    constructor(services, config, store) {
        this.services = services;
        this.config = config;
        this.store = store;
    }
}
