import {BaseRepository} from "./base.repository";
import { takeEvery, put, call } from 'redux-saga/effects';
import files from 'src/modules/files'

export class FilesRepository extends BaseRepository{
    init() {
        return [
            takeEvery(
                files.constants.FETCH_ALL_FILES,
                this.getAllFiles.bind(this)
            ),
            takeEvery(
                files.constants.FETCH_FILE,
                this.getFileDetails.bind(this)
            )
        ]
    }

    * getAllFiles(action) {
        yield put(files.actions.list.fetching(true))
        try {
            const data = yield call(
                [this.services.get('files'), 'getFiles'],
                files.queries.getAllFiles,
                action.payload
            )
            yield put(files.actions.list.update(data))
            yield put(files.actions.list.fetching(false))
        } catch (e) {
            console.error(e)
            yield put(files.actions.list.update([]))
            yield put(files.actions.list.fetching(false))
        }
    }

    * getFileDetails(action) {
        yield put(files.actions.details.fetching(true))
        try {
            const data = yield call(
                [this.services.get('files'), 'getFileDetails'],
                files.queries.getFileDetails,
                action.payload
            )
            yield put(files.actions.details.update(data))
            yield put(files.actions.details.fetching(false))
        } catch (e) {
            console.error(e)
            yield put(files.actions.details.update({}))
            yield put(files.actions.details.fetching(false))
        }
    }
}
