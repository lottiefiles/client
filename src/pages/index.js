import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import {MainLayout} from "../layouts/main";
import {FilterBar} from "../views/Home/FilterBar";
import {useDispatch} from "react-redux";
import files from 'src/modules/files'
import {useEffect, useState} from "react";
import {FilesList} from "../views/Home/FilesList";
import {Preview} from "../views/Home/Preview";

export default function Home() {
    const dispatch = useDispatch()

    const getFiles = (search, filter) => {
        dispatch(files.actions.list.fetch({
            search,
            filter
        }))
    }

    const onChange = (values) => {
        getFiles(values.search, values.filter)
    }

    useEffect(() => {
        getFiles('', '')
    }, [])

    useEffect(() => {
        import("@lottiefiles/lottie-player");
    });


    return (
        <MainLayout>
            <FilterBar onChange={onChange} />
            <FilesList/>
            <Preview/>
        </MainLayout>
    )
}
