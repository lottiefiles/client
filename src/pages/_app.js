import '../../styles/globals.css'

import {ApolloProvider} from "@apollo/client";
import {Provider} from 'react-redux';
import configureStore from 'src/store';
import states from 'src/states';

const store = configureStore(states);
import client from 'src/utils/graphql'

function MyApp({Component, pageProps}) {
    return (
        <ApolloProvider client={client}>
            <Provider store={store}>
                <Component {...pageProps} />
            </Provider>
        </ApolloProvider>
    )
}

export default MyApp
