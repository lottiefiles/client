export const common = {
    graphql: {
        url: process.env.NEXT_PUBLIC_GRAPHQL_URL
    },
    files: {
        url: process.env.NEXT_PUBLIC_FILES_URL
    }
}
