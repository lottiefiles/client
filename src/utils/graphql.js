import { ApolloClient, InMemoryCache } from "@apollo/client";
import config from 'src/config';

const client = new ApolloClient({
    uri: config.common.graphql.url,
    cache: new InMemoryCache(),
});

export default client;
