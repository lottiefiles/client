import React from 'react';
import Image from "next/image";
import {TopBar} from "../components/TopBar";

export const MainLayout = ({ children }) => {
    return (
        <div className="bg-gray-200">
            <TopBar/>
            {children}
        </div>
    )
}
