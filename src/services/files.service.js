export class FilesService {
    constructor(config, client) {
        this.config = config
        this.client = client
    }

    async getFiles(query, data) {
        try {
            const response =  await this.client.query({
                query,
                variables: data
            })
            return response.data.files || []
        } catch (e) {
            throw new Error('There is an error in query')
        }
    }

    async getFileDetails(query, fId) {
        try {
            const response =  await this.client.query({
                query,
                variables: {
                    fId: fId * 1
                }
            })
            return response.data.file || {}
        } catch (e) {
            console.error(e)
            throw new Error(`There is an file with id ${id}`)
        }
    }

}
