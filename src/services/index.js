import client from 'src/utils/graphql'
import config from '../config';

import {FilesService} from "./files.service";

class Services {
    constructor() {
        this.services = new Map();
        this.init();
    }

    init() {
        const files = new FilesService(config, client)
        this.add('files', files)
    }

    add(name, service) {
        this.services.set(name, service);
    }

    get(name) {
        if (!this.services.has(name)) {
            throw new Error('Service Not Exists');
        }
        return this.services.get(name);
    }
}

const services = new Services();
export default services;
