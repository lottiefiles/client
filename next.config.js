// next.config.js
const withGraphql = require('next-plugin-graphql')
module.exports = withGraphql({
  reactStrictMode: true,
})
