# LottieFiles Client

## Setup
```
git close git@gitlab.com:lottiefiles/client.gt
cd client
```

## Installing the packages
```
yarn install
```

## Running the project
```
yarn build
yarn start
```
